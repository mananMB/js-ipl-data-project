const writeFile = require("./writeFile");
const getIdOfMatchesPlayedInYear = require("./get-id-of-matches-played-in-year");

const extraRunsConcededPerTeam = (matches, deliveries, year) => {
  let extraRunsCollection = {};
  let extraRunsConcededPerTeam = [];
  let idOfMatchesPlayedInYear = getIdOfMatchesPlayedInYear(matches, year);

  deliveries.forEach((delivery) => {
    if (!idOfMatchesPlayedInYear.includes(delivery.match_id)) {
      return;
    }
    if (!(Number(delivery.extra_runs) !== 0)) {
      return;
    }
    if (!(delivery.batting_team in extraRunsCollection)) {
      extraRunsCollection[delivery.batting_team] = 0;
    }
    extraRunsCollection[delivery.batting_team] += Number(delivery.extra_runs);
  });

  Object.entries(extraRunsCollection).forEach((entry) => {
    extraRunsConcededPerTeam.push({ team: entry[0], extra_runs: entry[1] });
  });

  writeFile(extraRunsConcededPerTeam, "3-extra-runs-conceded-per-team");
};

module.exports = extraRunsConcededPerTeam;
