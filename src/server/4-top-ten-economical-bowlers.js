const getIdOfMatchesPlayedInYear = require("./get-id-of-matches-played-in-year");
const writeFile = require("./writeFile");
const getBowlerRunsAndBallsStats = require("./get-bowler-runs-and-balls-stats");
const generateEconomyRate = require("./generate-economy-rate");

const topTenEconomicalBowlers = (matches, deliveries, year) => {
  const idOfMatchesPlayedInYear = getIdOfMatchesPlayedInYear(matches, year);
  const bowlerStats = {};
  const economyRateArray = [];
  const topTenEconomyRate = [];
  deliveries.forEach((delivery) => {
    if (idOfMatchesPlayedInYear.includes(delivery.match_id)) {
      getBowlerRunsAndBallsStats(bowlerStats, delivery);
    }
  });

  generateEconomyRate(bowlerStats, economyRateArray);

  economyRateArray.slice(0, 10).forEach((value, index) => {
    topTenEconomyRate.push({
      bowler: economyRateArray[index][0],
      economy_rate: economyRateArray[index][1].toFixed(2),
    });
    return true;
  });

  writeFile(topTenEconomyRate, "4-top-ten-economical-bowlers");
};

module.exports = topTenEconomicalBowlers;
