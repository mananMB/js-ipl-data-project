const writeFile = require("./writeFile");
const matchesWonPerTeamPerYear = (matches) => {
  let matchWinCollection = {};
  let matchesWonPerTeamPerYear = [];

  matches.forEach((match) => {
    if (match.winner === "") {
      return;
    }
    if (!(match.winner in matchWinCollection)) {
      matchWinCollection[match.winner] = {};
    }
    if (!(match.season in matchWinCollection[match.winner])) {
      matchWinCollection[match.winner][match.season] = 0;
    }
    matchWinCollection[match.winner][match.season] += 1;
  });

  Object.entries(matchWinCollection).forEach((entry) => {
    matchesWonPerTeamPerYear.push({
      team: entry[0],
      wins_per_season: entry[1],
    });
  });

  writeFile(matchesWonPerTeamPerYear, "2-matches-won-per-team-per-year");
};

module.exports = matchesWonPerTeamPerYear;
