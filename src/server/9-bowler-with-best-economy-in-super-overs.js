const writeFile = require("./writeFile");
const getBowlerRunsAndBallsStats = require("./get-bowler-runs-and-balls-stats");
const generateEconomyRate = require("./generate-economy-rate");

const bowlerWithBestEconomyInSuperOver = (deliveries) => {
  const bowlerStats = {};
  const economyRateArray = [];
  let topEconomyRate;
  deliveries.forEach((delivery) => {
    if (Number(delivery.is_super_over)) {
      getBowlerRunsAndBallsStats(bowlerStats, delivery);
    }
  });

  generateEconomyRate(bowlerStats, economyRateArray);

  topEconomyRate = [
    {
      bowler: economyRateArray[0][0],
      economy_rate: economyRateArray[0][1],
    },
  ];

  writeFile(topEconomyRate, "9-bowler-with-best-economy-in-super-overs");
};

module.exports = bowlerWithBestEconomyInSuperOver;
