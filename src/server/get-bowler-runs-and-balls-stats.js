const getBowlerRunsAndBallsStats = (bowlerStats, delivery) => {
  if (!bowlerStats.hasOwnProperty(delivery.bowler)) {
    bowlerStats[delivery.bowler] = {};
  }
  if (!("total_runs" in bowlerStats[delivery.bowler])) {
    bowlerStats[delivery.bowler].total_runs = 0;
    bowlerStats[delivery.bowler].balls_bowled = 0;
  }
  bowlerStats[delivery.bowler].total_runs += Number(delivery.total_runs);
  bowlerStats[delivery.bowler].balls_bowled += 1;
};

module.exports = getBowlerRunsAndBallsStats;
