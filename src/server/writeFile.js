const fs = require("fs");

const writeFile = (data, fileName) => {
  fs.writeFile(
    `src/public/output/${fileName}.json`,
    JSON.stringify(data, null, 2),
    (err) => {
      if (err) {
        console.log(`Error writing ${fileName} File`);
        console.log(err);
      }
    }
  );
};

module.exports = writeFile;
