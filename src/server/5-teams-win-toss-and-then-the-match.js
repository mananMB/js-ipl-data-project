const writeFile = require("./writeFile");
const teamsWinTossAndThenTheMatch = (matches) => {
  const tossAndMatchWinCollection = {};
  const teamsWinTossAndThenTheMatch = [];

  matches.forEach((match) => {
    if (match.toss_winner !== match.winner) {
      return;
    }
    if (!(match.toss_winner in tossAndMatchWinCollection)) {
      tossAndMatchWinCollection[match.toss_winner] = 0;
    }
    tossAndMatchWinCollection[match.toss_winner] += 1;
  });

  Object.entries(tossAndMatchWinCollection).forEach((entry) => {
    teamsWinTossAndThenTheMatch.push({ team: entry[0], wins: entry[1] });
  });

  writeFile(teamsWinTossAndThenTheMatch, "5-teams-win-toss-and-then-the-match");
};

module.exports = teamsWinTossAndThenTheMatch;
