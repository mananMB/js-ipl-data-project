const generateEconomyRate = (bowlerStats, economyRateArray) => {
  Object.keys(bowlerStats).forEach((bowler) => {
    economyRateArray.push([
      bowler,
      bowlerStats[bowler].total_runs / (bowlerStats[bowler].balls_bowled / 6),
    ]);
  });

  economyRateArray.sort((a, b) => {
    return a[1] - b[1];
  });
};

module.exports = generateEconomyRate;
