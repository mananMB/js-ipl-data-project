const writeFile = require("./writeFile");

const mostPOTMPerSeason = (matches) => {
  const mostPOTMPerSeasonCollection = {};
  const mostPOTMPerSeason = [];
  const POTMPerSeason = {};

  matches.forEach((match) => {
    if (!POTMPerSeason.hasOwnProperty(match.season)) {
      POTMPerSeason[match.season] = {};
    }
    if (!(match.player_of_match in POTMPerSeason[match.season])) {
      POTMPerSeason[match.season][match.player_of_match] = 0;
    }
    POTMPerSeason[match.season][match.player_of_match] += 1;
  });

  Object.keys(POTMPerSeason).forEach((season) => {
    mostPOTMPerSeasonCollection[season] = Object.fromEntries([
      Object.entries(POTMPerSeason[season]).reduce(
        (initialValue, currentValue) => {
          if (initialValue[1] > currentValue[1]) {
            return initialValue;
          }
          return currentValue;
        },
        [undefined, 0]
      ),
    ]);
  });

  Object.entries(mostPOTMPerSeasonCollection).forEach((entry) => {
    Object.entries(entry[1]).forEach((count) => {
      mostPOTMPerSeason.push({
        year: entry[0],
        player: count[0],
        wins: count[1],
      });
    });
  });

  writeFile(mostPOTMPerSeason, "6-most-POTM-per-season");
};

module.exports = mostPOTMPerSeason;
