const writeFile = require("./writeFile");

const matchesPlayedPerYear = (matches) => {
  let matchesPlayedPerYear = Object.entries(
    matches.reduce((matchCountAcc, match) => {
      if (!(match.season in matchCountAcc)) {
        matchCountAcc[match.season] = 0;
      }
      matchCountAcc[match.season] += 1;

      return matchCountAcc;
    }, {})
  ).map((entry, index) => {
    return { year: entry[0], matches_count: entry[1] };
  });
  writeFile(matchesPlayedPerYear, "1-matches-played-per-year");
};

module.exports = matchesPlayedPerYear;
