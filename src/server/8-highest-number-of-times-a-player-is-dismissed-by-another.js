const writeFile = require("./writeFile");
const highestNumberOfTimesAPlayerIsDismissedByAnother = (deliveries) => {
  let highestNumberOfTimesAPlayerIsDismissedByAnother;
  const playerDismissStat = {};
  let reducedTimesAPlayerIsDismissedByAnother = {};

  deliveries.forEach((delivery) => {
    if (!(delivery.batsman === delivery.player_dismissed)) {
      return;
    }
    if (!(delivery.batsman in playerDismissStat)) {
      playerDismissStat[delivery.batsman] = {};
    }
    if (!(delivery.bowler in playerDismissStat[delivery.batsman])) {
      playerDismissStat[delivery.batsman][delivery.bowler] = 0;
    }
    playerDismissStat[delivery.batsman][delivery.bowler] += 1;
  });

  Object.keys(playerDismissStat).forEach((player) => {
    reducedTimesAPlayerIsDismissedByAnother[player] = Object.entries(
      playerDismissStat[player]
    ).reduce(
      (initialValue, currentValue) => {
        if (initialValue[1] > currentValue[1]) {
          return initialValue;
        }
        return currentValue;
      },
      [undefined, 0]
    );
  });

  let highest;

  Object.keys(reducedTimesAPlayerIsDismissedByAnother).forEach((player) => {
    highest = Object.entries(reducedTimesAPlayerIsDismissedByAnother).reduce(
      (initialValue, currentValue) => {
        if (initialValue[1][1] > currentValue[1][1]) {
          return initialValue;
        }
        return currentValue;
      },
      [undefined, [undefined, 0]]
    );
  });

  highestNumberOfTimesAPlayerIsDismissedByAnother = [
    {
      player: highest[0],
      dismissed_by: highest[1][0],
      times: highest[1][1],
    },
  ];

  writeFile(
    highestNumberOfTimesAPlayerIsDismissedByAnother,
    "8-highest-number-of-times-a-player-is-dismissed-by-another"
  );
};

module.exports = highestNumberOfTimesAPlayerIsDismissedByAnother;
