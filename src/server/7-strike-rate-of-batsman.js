const getIdOfMatchesPlayedInYear = require("./get-id-of-matches-played-in-year");
const writeFile = require("./writeFile");

const strikeRateOfBatsman = (matches, deliveries, batsman) => {
  const collection = {}; //strike rate collection
  const strikeRateOfBatsman = [];

  let matchYear = Object.fromEntries(
    matches.map((match) => {
      return [match.id, match.season];
    })
  );

  deliveries.forEach((delivery) => {
    let season = matchYear[delivery.match_id];
    if (!(delivery.batsman in collection)) {
      collection[delivery.batsman] = {};
    }
    if (!(season in collection[delivery.batsman])) {
      collection[delivery.batsman][season] = {};
    }
    if (!("runs_scored" in collection[delivery.batsman][season])) {
      collection[delivery.batsman][season].runs_scored = 0;
      collection[delivery.batsman][season].balls_played = 0;
    }
    collection[delivery.batsman][season].runs_scored += Number(
      delivery.batsman_runs
    );
    if (Number(delivery.wide_runs) === 0)
      collection[delivery.batsman][season].balls_played += 1;
  });

  Object.keys(collection).forEach((batsman) => {
    Object.keys(collection[batsman]).forEach((season) => {
      collection[batsman][season].strike_rate = (
        (collection[batsman][season].runs_scored /
          collection[batsman][season].balls_played) *
        100
      ).toFixed(2);
    });
  });

  Object.entries(collection).forEach((entry) => {
    strikeRateOfBatsman.push({ player: entry[0], stats: entry[1] });
  });

  writeFile(strikeRateOfBatsman, "7-strike-rate-of-batsman");
};

module.exports = strikeRateOfBatsman;
