const getIdOfMatchesPlayedInYear = (matches, year) => {
  let idList = [];
  matches.forEach((match) => {
    if (Number(match.season) === Number(year)) {
      idList.push(match.id);
    }
  });

  return idList;
};

module.exports = getIdOfMatchesPlayedInYear;
