const { parse } = require("csv-parse");
const fs = require("fs");
const { finished } = require("stream/promises");
const matchesPerYear = require("./1-matches-played-per-year");
const matchesWonPerTeam = require("./2-matches-won-per-team-per-year");
const extraRunsConcededPerTeam = require("./3-extra-runs-conceded-per-team");
const topTenEconomicalBowlers = require("./4-top-ten-economical-bowlers");
const teamsWinTossAndThenTheMatch = require("./5-teams-win-toss-and-then-the-match");
const mostPOTMPerSeason = require("./6-most-POTM-per-season");
const strikeRateOfBatsman = require("./7-strike-rate-of-batsman");
const bowlerWithBestEconomyInSuperOver = require("./9-bowler-with-best-economy-in-super-overs");
const highestNumberOfTimesAPlayerIsDismissedByAnother = require("./8-highest-number-of-times-a-player-is-dismissed-by-another");

const processFile = async (filePath) => {
  const records = [];
  const parser = fs.createReadStream(filePath).pipe(
    parse({
      columns: true,
    })
  );

  parser.on("readable", function () {
    let record;
    while ((record = parser.read()) !== null) {
      records.push(record);
    }
  });

  await finished(parser);
  return records;
};

processFile("src/data/matches.csv").then((matches) => {
  processFile("src/data/deliveries.csv").then((deliveries) => {
    matchesPerYear(matches);
    matchesWonPerTeam(matches);
    extraRunsConcededPerTeam(matches, deliveries, 2016);
    topTenEconomicalBowlers(matches, deliveries, 2015);
    teamsWinTossAndThenTheMatch(matches);
    mostPOTMPerSeason(matches);
    strikeRateOfBatsman(matches, deliveries);
    highestNumberOfTimesAPlayerIsDismissedByAnother(deliveries);
    bowlerWithBestEconomyInSuperOver(deliveries);
  });
});
